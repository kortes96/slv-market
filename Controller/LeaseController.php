<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Repository\SlavesRepository;
use App\Repository\MastersRepository;
use App\Repository\ContractsRepository;
use App\Helper\LeaseHelper;
use App\Request\LeaseRequest;
use App\Response\LeaseResponse;

/**
 * Аренда раба
 *
 * @package SlaveMarket\Controller;
 */
class LeaseController
{
    /**
     * @var SlavesRepository
     */
    protected $slavesRepository;

    /**
     * @var MastersRepository
     */
    protected $mastersRepository;

    /**
     * @var ContractsRepository
     */
    protected $contractsRepository;

    /**
     * @var LeaseHelper
     */
    protected $leaseHelper;

    /**
     * LeaseController constructor
     *
     * @param SlavesRepository $slavesRepository
     * @param MastersRepository $mastersRepository
     * @param ContractsRepository $contractsRepository
     * @param LeaseHelper $leaseHelper
     */
    public function __construct(
        SlavesRepository $slavesRepository, 
        MastersRepository $mastersRepository, 
        ContractsRepository $contractsRepository, 
        LeaseHelper $leaseHelper
    )
    {
        $this->slavesRepository    = $slavesRepository;
        $this->mastersRepository   = $mastersRepository;
        $this->contractsRepository = $contractsRepository;
    }

    /**
     * Аренда раба
     *
     * @param LeaseRequest $request
     * @return LeaseResponse
     */
    public function run(LeaseRequest $request): LeaseResponse
    {
        $response = new LeaseResponse();

        $master = $this->mastersRepository->findById($request->masterId);
        $slave  = $this->slavesRepository->findById($request->slaveId);

        $response->addError($this->leaseHelper->getSlaveWorkHours($request->timeFrom, $request->timeTo));

        if ($slaveContracts = $this->contractsRepository->findSlaveContracts($slave->getId(), $request->timeFrom, $request->timeTo)) {
            $existingContractMaster = $this->mastersRepository->findById($slaveContracts->getMasterId());

            $response->addError(
                $this->leaseHelper->checkPossibilityLeaseContract(
                    $master, 
                    $existingContractMaster, 
                    $slaveContracts, 
                    $request->timeFrom, 
                    $request->timeTo
                )
            );

            if (empty($response->getErrors())) {
                $price = $this->leaseHelper->getLeasePrice($slave->getPricePerHour(), $request->timeFrom, $request->timeTo);
                $startLeaseHour = date('H', strtotime( $request->timeFrom ));
                $startLease = date('Y-m-d H', strtotime( $request->timeFrom));
                $stopLeaseHour  = date('H', strtotime( $request->timeTo));
                if ( $startLeaseHour === $stopLeaseHour ) {
                    $stopLease = date('Y-m-d H', strtotime($request->timeTo) + 3600);
                } else {
                    $stopLease = date('Y-m-d H', strtotime($request->timeTo));
                }

                $contract = new Contracts($master->getId(), $slave->getId(), $price, $startLease, $stopLease);

                $this->response->setLeaseContract($contract);
            }
        }

        return $this->response;
    }
}
