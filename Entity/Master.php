<?php

namespace App\Entity;

/**
 * Хозяин
 * 
 * @package SlaveMarket\Entity
 */
class Master
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @Description Имя хозяина
     * @var string
     */
    protected $name;

    /**
     * @Description Наличие vip статуса
     * @var boolean
     */
    protected $isVip;

    /**
     * Master constructor
     *
     * @param int $id
     * @param string $name
     * @param bool $isVip
     */
    public function __construct(int $id, string $name, bool $isVip = false)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->isVip = $isVip;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @Description Проверка vip статуса
     * @return bool
     */
    public function getIsVip(): bool
    {
        return $this->isVip;
    }
}
