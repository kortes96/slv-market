<?php

namespace App\Entity\Slave;

/**
 * Раб
 * 
 * @package SlaveMarket\Entity
 */
class Slave
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @Description Имя раба
     * @var string
     */
    protected $name;

    /**
     * @Description Стоимость за час работы
     * @var float
     */
    protected $pricePerHour;

    /**
     * Slave constructor
     *
     * @param int $id
     * @param string $name
     * @param float $pricePerHour
     */
    public function __construct(int $id, string $name, float $pricePerHour)
    {
        $this->id           = $id;
        $this->name         = $name;
        $this->pricePerHour = $pricePerHour;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPricePerHour(): float
    {
        return $this->pricePerHour;
    }
}
