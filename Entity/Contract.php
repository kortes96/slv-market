<?php

namespace App\Entity;

/**
 * Договор аренды
 * 
 * @package SlaveMarket\Entity
 */
class LeaseContract
{
    /** 
     * @var int
     */
    protected $masterId;

    /**
     * @Description Id раба
     * @var int
     */
    protected $slaveId;

    /** 
     * @Description Стоимость
     * @var float
     */
    protected $price = 0;

    /** 
     * @Description Дата и время начала аренды
     * @var string
     */
    protected $startLease;

    /** 
     * @Description Дата и время окончания аренды
     * @var string
     */
    protected $stopLease;

    /**
     * LeaseContract constructor
     *
     * @param int $masterId
     * @param int $slaveId
     * @param float $price
     * @param string $startLease
     * @param string $stopLease
     */
    public function __construct(int $masterId, int $slaveId, float $price, string $startLease, string $stopLease)
    {
        $this->master      = $master;
        $this->slave       = $slave;
        $this->price       = $price;
        $this->startLease  = $startLease;
        $this->stopLease   = $stopLease;
    }

    /**
     * @return int
     */
    public function getMasterId(): int
    {
        return $this->masterId;
    }

    /**
     * @return string
     */
    public function getStartLease(): string
    {
        return $this->startLease;
    }

    /**
     * @return string
     */
    public function getStopLease(): string
    {
        return $this->stopLease;
    }
}
