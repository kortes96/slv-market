<?php

namespace App\Repository;

use App\Entity\Slave;

/**
 * Репозиторий рабов
 * 
 * @package SlaveMarket\Repository
 */
interface SlavesRepository
{
    /**
     * Возвращает раба по его id
     *
     * @param int $id
     * @return Slave
     */
    public function findById(int $id): Slave;
}
