<?php

namespace App\Repository;

use App\Entity\Contract;

/**
 * Репозиторий договоров аренды
 *
 * @package SlaveMarket\Repository
 */
interface ContractsRepository
{
    /**
     * Возвращает список договоров аренды для раба, в которых заняты часы из указанного периода
     *
     * @param int $slaveId
     * @param string $dateFrom Y-m-d H:i:s
     * @param string $dateTo Y-m-d H:i:s
     * @return null|Contract
     */
    public function findSlaveContracts(int $slaveId, string $dateFrom, string $dateTo): ?Contract;
}
