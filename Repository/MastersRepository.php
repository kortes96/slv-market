<?php

namespace App\Repository;

use App\Entity\Master;

/**
 * Репозиторий хозяев
 *
 * @package SlaveMarket\Repository
 */
interface MastersRepository
{
    /**
     * Возвращает хозяина по его id
     *
     * @param int $id
     * @return Master
     */
    public function findById(int $id): Master;
}
