<?php

namespace App\Helper;

/**
 * Проверка возможности аренды
 * 
 * @package SlaveMarket\Helper
 */
class LeaseHelper
{
    /**
     * @Description Максимальное количество часов работы раба в день
     * @var int
     */
    protected $maxWorkHoursPerDay = 16;

    /**
     * @Description Час начала рабочего дня
     * @var int
     */
    protected $startWorkDayHour = 0;

    /**
     * @Description Час окончания рабочего дня
     * @var int
     */
    protected $stopWorkDayHour = 0;

    /**
     * @Description 
     *
     * @return mixed
     */
    public function getFullDaysLease(string $timeFrom, string $timeTo) {
        $timeFrom = \DateTime::createFromFormat("Y-m-d H:i:s", $timeFrom);
        $timeTo   = \DateTime::createFromFormat("Y-m-d H:i:s", $timeTo);

        return $timeFrom->diff($timeTo)->days;
    }

    /**
     * @param int $hourFrom
     * @param int $hourTo
     *
     * @return null|string
     */
    private function checkMaxWorkHoursPerDay(int $hourFrom, int $hourTo): ?string 
    {
        if ($hourTo === $hourFrom) {
            $hourTo++;
        }

        if (($hourTo - $hourFrom) > $this->maxWorkHoursPerDay) {
            return 'Превышено максимальное количество часов работы раба в день (' . $this->maxWorkHoursPerDay . ')';
        }
    }

    /**
     * @param string $timeFrom in format Y-m-d H:i:s
     * @param string $timeTo in format Y-m-d H:i:s
     *
     * @return null|string
     */
    public function getSlaveWorkHours(string $timeFrom, string $timeTo): ?string {

        $fullDays = $this->getFullDaysLease($timeFrom, $timeTo);

        if ($fullDays) {
            return $this->checkMaxWorkHoursPerDay(date($this->startWorkDayHour, date('H', strtotime($timeTo))));
        }
        else {
            return $this->checkMaxWorkHoursPerDay(date('H', strtotime($timeFrom)), date('H', strtotime($timeTo)));
        }
    }

    /**
     * @param Master $master
     * @param Master $existingContractMaster
     * @param Contracts $slaveContracts
     * @param string $timeFrom in format Y-m-d H:i:s
     * @param string $timeTo in format Y-m-d H:i:s
     *
     * @return null|string
     */
    public function checkPossibilityLeaseContract(Master $master, Master $existingContractMaster, Contracts $slaveContracts, string $timeFrom, string $timeTo ): ?string {

        $error = null;

        $existingStartLease = strtotime($slaveContracts->getStartLease());
        $existingStopLease = strtotime($slaveContracts->getStopLease());

        $startHour = date('H', strtotime($timeFrom));
        $startTime = strtotime(date('Y-m-d H:00:00', strtotime($timeFrom)));
        $stopHour = date('H', strtotime($timeTo));
        if ( $startHour === $stopHour ) {
            $stopTime = strtotime(date('Y-m-d H:00:00', strtotime($timeTo) + 3600));
        } else {
            $stopTime = strtotime(date('Y-m-d H:00:00', strtotime($timeTo)));
        }

        if (($existingStartLease < $startHour && $startHour < $existingStopLease ) || ($existingStartLease < $stopTime && $stopTime < $existingStopLease)) {
            $error = 'Аренда раба ' . $slaveContracts->getSlaveName() . ' невозможна. Раб занят с ' . $slaveContracts->getStartLease() . ' по ' . $slaveContracts->getStopLease() . '.';
        }

        if ( $error && (!$master->getIsVip() || $existingContractMaster->getIsVip())) {
            return $error;
        }
    }

    /**
     * @param float $pricePerHour
     * @param string $startLease
     * @param string $stopLease
     *
     * @return float
     */
    public function getLeasePrice(float $pricePerHour, string $startLease, string $stopLease): float 
    {
        $fullDays  = $this->getFullDaysLease($startLease, $stopLease);
        $startHour = (int) date('H', strtotime($startLease));
        $stopHour  = (int) date('H', strtotime($stopLease));
        if ( $startHour === $stopHour ) {
            $stopHour ++;
        }

        if ($fullDays === 0) {
            $totalHour = $stopHour - $startHour;
        } else {
            $totalHour = ($this->stopWorkDayHour - $startHour) + ($stopHour - $this->hourStartWorkDay) + ( $fullDays - 1) * $this->maxWorkHoursPerDay;
        }

        return $pricePerHour * $totalHour;
    }
}
