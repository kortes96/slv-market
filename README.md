Реализовано только то, что требовалось для решения задачи аренды
- модели Slave, Master и Contract (+ репозитории с заглушками)

Запуск приложения в /src/Controller/LeaseController.php

Проверка возможности аренды вынесена в /src/Helper/LeaseHelper.php

Структура таблиц БД для 2-ой части задания:

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE `category_slave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11),
  `slave_id` int(11),
  PRIMARY KEY (`id`)
  UNIQUE KEY `category_id_slave_id` (`category_id`,`slave_id`),
  KEY `FK_SLAVE` (`slave_id`),
  CONSTRAINT `FK_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_SLAVE` FOREIGN KEY (`slave_id`) REFERENCES `slave` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `slave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - мужчина/1 - женщина',
  `weight` int(11) NOT NULL COMMENT 'вес, кг',
  `price_per_hour` decimal(10,2) NOT NULL COMMENT 'стоимость часа работы',
  `price` decimal(10,2) NOT NULL COMMENT 'стоимость раба',
  PRIMARY KEY (`id`),
  KEY `price` (`price`),
  KEY `weight` (`weight`),
  KEY `gender` (`gender`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

Получить минимальную, максимальную и среднюю стоимость всех рабов весом более 60 кг.

SELECT MIN(price) AS min, AVG(price) AS avg, MAX(price) AS max 
FROM slave 
WHERE weight > 60


Выбрать категории, в которых больше 10 рабов.

SELECT c.id, c.name 
FROM category c 
INNER JOIN category_slave cs ON c.id = cs.category_id 
GROUP BY c.id
HAVING COUNT(c.id) > 10


Выбрать категорию с наибольшей суммарной стоимостью рабов.

SELECT rs.id, rs.name, MAX(rs.sum) 
FROM (
    SELECT c.id, c.name AS name, SUM(s.price) AS sum 
    FROM category c 
    INNER JOIN category_slave cs ON c.id = cs.category_id 
    INNER JOIN slave s ON cs.slave_id = s.id 
    GROUP BY c.id
    ) 
AS rs


Выбрать категории, в которых мужчин больше чем женщин.

SELECT c.id, c.name
FROM category c 
INNER JOIN category_slave cs ON c.id = cs.category_id 
LEFT JOIN slave s ON cs.slave_id = s.id AND s.gender = 0 
LEFT JOIN slave ss ON cs.slave_id = ss.id AND ss.gender = 1 
GROUP BY c.id
HAVING COUNT(s.gender) > COUNT(ss.gender)


Количество рабов в категории "Для кухни" (включая все вложенные категории).
Считаем, что id категории "Для кухни" равен 1

-
