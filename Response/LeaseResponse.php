<?php

namespace App\Response;

use App\Entity\Contract;

/**
 * Результат попытки аренды
 * @package SlaveMarket\Response
 */
class LeaseResponse
{
    /**
     * @Description Договор аренды
     * @var Contract
     */
    protected $leaseContract;

    /**
     * @Description Список ошибок
     * @var Contract
     */
    protected $errors = [];

    /**
     * @Description Возвращает договор аренды, если аренда была успешной
     * @return Contract
     */
    public function getLeaseContract(): ?Contract
    {
        return $this->leaseContract;
    }

    /**
     * @Description Указать договор аренды
     * @param Contract $leaseContract
     */
    public function setLeaseContract(Contract $leaseContract)
    {
        $this->leaseContract = $leaseContract;
    }

    /**
     * @Description Добавить ошибку
     * @param string $message
     */
    public function addError(string $message): void
    {
        $this->errors[] = $message;
    }

    /**
     * @Description Возвращает все ошибки аренды
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
