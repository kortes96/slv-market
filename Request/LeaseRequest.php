<?php

namespace App\Request;

/**
 * Запрос на аренду раба
 * 
 * @package SlaveMarket\Request
 */
class LeaseRequest
{
    /** 
     * @Description id хозяина
     * @var int 
     */
    public $masterId;

    /** 
     * @Description id раба
     * @var int 
     */
    public $slaveId;

    /** 
     * @Description время начала аренды Y-m-d H:i:s
     * @var string 
     */
    public $timeFrom;

    /** 
     * @Description время окончания аренды Y-m-d H:i:s
     * @var string 
     */
    public $timeTo;

	/**
	 * @param int $masterId
	 * @param int $slaveId
	 * @param string $timeFrom in format Y-m-d H:i:s
	 * @param string $timeTo in format Y-m-d H:i:s
	 */
	public function __construct(int $masterId, int $slaveId, string $timeFrom, string $timeTo) {
		$this->masterId = $masterId;
		$this->slaveId  = $slaveId;
		$this->timeFrom = $timeFrom;
		$this->timeTo   = $timeTo;
	}
}